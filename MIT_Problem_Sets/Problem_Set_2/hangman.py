# Problem Set 2, hangman.py
# Name: 
# Collaborators:
# Time spent:

# Hangman Game
# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)
import random
import string

WORDLIST_FILENAME = "words.txt"


def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.
    
    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist



def choose_word(wordlist):
    """
    wordlist (list): list of words (strings)
    
    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code

# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = load_words()


def is_word_guessed(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing; assumes all letters are
      lowercase
    letters_guessed: list (of letters), which letters have been guessed so far;
      assumes that all letters are lowercase
    returns: boolean, True if all the letters of secret_word are in letters_guessed;
      False otherwise
    '''
    j=0
    for k in range(len(letters_guessed)):
        for i in secret_word:
            if i==letters_guessed[k]:
                j+=1
    if j == len(secret_word):
        return True
    else:
        return False

def get_guessed_word(secret_word, letters_guessed):
    '''
    secret_word: string, the word the user is guessing
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string, comprised of letters, underscores (_), and spaces that represents
      which letters in secret_word have been guessed so far.
    '''
    word = ''
    k=0
    s=0
    t=0
    for i in range(len(secret_word)):
        l=word
        for j in range(len(letters_guessed)):
            if secret_word[i]==letters_guessed[j]:
                #k+=1
                word+=letters_guessed[j]
                break
        if word==l:    #if l==k:
            word+= "_ "
        if secret_word[i]==letters_guessed[-1]:
            k+=1
        if letters_guessed[-1]=='a' or letters_guessed[-1]=='e' or letters_guessed[-1]=='i' or letters_guessed[-1]=='o' or letters_guessed[-1]=='u':
                s+=1
    if k>0:
        word = "Good guess:",word,
        return word, 1
    elif k==0 and s>0:
        word = "Ooh sorry, that letter is not in my word:",word,
        return word,2
    else:
        word = "Ooh sorry, that letter is not in my word:",word,
        return word, 3

def get_available_letters(letters_guessed):
    '''
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string (of letters), comprised of letters that represents which letters have not
      yet been guessed.
    '''
    word = string.ascii_lowercase
    for j in range(len(letters_guessed)):
        for i in string.ascii_lowercase:
            if i == letters_guessed[j]:
                word = word.replace(letters_guessed[j],"")    
    return word        

def print_rules():
    print("A word is randomly chosen for you to guess, the length of which is told to you.")
    print("Enter a letter when instructed and you will be told if your guess is in the word or not.")
    print("If your letter is in the word, the position of the guessed letter in the word will be shown.")
    print("If your letter isn't in the word, you will lose a guess.")
    print("You have 6 guesses before you lose the game.")
    print("You also have 3 warnings. You lose a warning everytime you enter a symbol not in the alphabet or a letter you have already guessed.")
    print("If you use all 3 of your warnings, another offense will lose you a guess.")
    print("You win if you successfully guess all the letters in the word.")
    print("You will then be given a score based on the number of guesses you had remaining and the number of unique letters in the word if you win.")
    print("Otherwise you will told what the word was you failed to guess.")
    print("Good luck and enjoy!")
    print("   ")
    return


def hangman(secret_word):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many 
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses

    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Remember to make
      sure that the user puts in a letter!
    
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
    
    Follows the other limitations detailed in the problem write-up.
    '''
    i=6
    l=3
    letters_guessed = []
    rules = input("Welcome to Hangman! Would you like to read the rules? Type y/n.")
    while True:
        if rules == "y":
            print_rules()
            break
        elif rules == "n":
            break
        else:
            print("You have not entered a valid input, type y/n for if you want to read the rules.")
    print("I am thinking of a word that is",len(secret_word),"long.")
    print("You have", l ," warnings remaining.")
    while True:
        print("----------")
        if i<=0:
            print("Sorry, you are out of guesses.")
            print("The word was", secret_word)
            break
        print("You have",i,"guesses left.")
        print("Available letters:", get_available_letters(letters_guessed))
        guess = input("Please guess a letter.").lower()
        test=0
        for k in range(len(string.ascii_lowercase)):    
            if guess != string.ascii_lowercase[k]:
                test +=1
            try:
                if guess == letters_guessed[k]:
                    test = False
                    break
            except IndexError:
                continue
        print(test)        
        if test == 26:
            if l>0:
                l-=1
                print("That is an invalid character. You now have ",l,"warnings left.")
                continue
            else:
                i-=1
                print("That is an invalid character. You are out of warnings so you have", i, "guesses left.")
                continue
        if test == False:
            if l>0:
                l-=1
                print("You have already guessed that letter. You now have",l,"warnings left.")
                continue
            else:
                i-=1
                print("You have already guessed that letter. You are out of warnings so you have",i,"guesses left.")
                continue
        letters_guessed.append(guess)        
        guess_word,guess_test = get_guessed_word(secret_word,letters_guessed)        
        print(guess_word)
        if guess_test == 2:
            i-=2
        if guess_test == 3:
            i-=1
        if is_word_guessed(secret_word,letters_guessed):
            print("Congratulations, you win!")
            o=0
            p=[]
            r=0
            for m in range(len(secret_word)):
                for n in string.ascii_lowercase:
                    if n==secret_word[m]:
                        o+=1
                        r=0
                        for q in range(len(p)):
                            if n==p[q]:
                                o-=1
                                r+=1
                        if r==0:
                            p.append(n)
            print("Your score is", i*o, ".")
            break
# When you've completed your hangman function, scroll down to the bottom
# of the file and uncomment the first two lines to test
#(hint: you might want to pick your own
# secret_word while you're doing your own testing)


# -----------------------------------



def match_with_gaps(my_word, other_word):
    '''
    my_word: string with _ characters, current guess of secret word
    other_word: string, regular English word
    returns: boolean, True if all the actual letters of my_word match the 
        corresponding letters of other_word, or the letter is the special symbol
        _ , and my_word and other_word are of the same length;
        False otherwise: 
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    pass



def show_possible_matches(my_word):
    '''
    my_word: string with _ characters, current guess of secret word
    returns: nothing, but should print out every word in wordlist that matches my_word
             Keep in mind that in hangman when a letter is guessed, all the positions
             at which that letter occurs in the secret word are revealed.
             Therefore, the hidden letter(_ ) cannot be one of the letters in the word
             that has already been revealed.

    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    pass



def hangman_with_hints(secret_word):
    '''
    secret_word: string, the secret word to guess.
    
    Starts up an interactive game of Hangman.
    
    * At the start of the game, let the user know how many 
      letters the secret_word contains and how many guesses s/he starts with.
      
    * The user should start with 6 guesses
    
    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.
    
    * Ask the user to supply one guess per round. Make sure to check that the user guesses a letter
      
    * The user should receive feedback immediately after each guess 
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the 
      partially guessed word so far.
      
    * If the guess is the symbol *, print out all words in wordlist that
      matches the current guessed word. 
    
    Follows the other limitations detailed in the problem write-up.
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"
    pass



# When you've completed your hangman_with_hint function, comment the two similar
# lines above that were used to run the hangman function, and then uncomment
# these two lines and run this file to test!
# Hint: You might want to pick your own secret_word while you're testing.


if __name__ == "__main__":
    # pass

    # To test part 2, comment out the pass line above and
    # uncomment the following two lines.
    
    while True:
        secret_word = choose_word(wordlist)
        hangman(secret_word)
        again = input("Would you like to play again? Type y/n").lower()
        if again == 'n':
            break
    

###############
    
    # To test part 3 re-comment out the above lines and 
    # uncomment the following two lines. 
    
    #secret_word = choose_word(wordlist)
    #hangman_with_hints(secret_word)
