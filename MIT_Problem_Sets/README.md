This directory contains my solutions to the MIT problem sets for the Introduction to Computer Science and Programming in Python course.

The information of what each problem set required is explained in the accompanying pdf in each subdirectory.