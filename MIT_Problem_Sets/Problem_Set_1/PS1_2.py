#Calculating how long it would take for someone to be able to afford a down payment on their dream house (price = total_cost) assuming they save a "portion_saved" percentage of their annual salary every month and taking into account annual return on investments.

import sys

def savetime(annual_salary,portion_saved,total_cost,current_savings,semi_annual_raise):
    i=0
    while i<=36:
        i+=1
        current_savings+=current_savings*0.04/12
        current_savings+=portion_saved*annual_salary/12
        if i==36 and abs(current_savings - total_cost*0.25) > 100:
            return True if current_savings-total_cost*0.25 > 100 else False
        if i==36 and abs(current_savings-total_cost*0.25) < 100:
            return portion_saved
        if i%6==0:
            annual_salary+=semi_annual_raise*annual_salary

current_savings=0
annual_salary = float(input("What is your annual salary? "))
high = 10000
low=0
total_cost = 1000000
semi_annual_raise= 0.07
bisection_times = 0
while True:
    portion_saved = (high+low)/(2*10000)
    i = savetime(annual_salary,portion_saved,total_cost,current_savings,semi_annual_raise)
    bisection_times+=1
    if bisection_times>200:
        print("There is no amount you could save per month to afford a down payment on the house in 3 years.")
        exit()
    elif i:
        high=portion_saved*10000
    elif i==False:
        low=portion_saved*10000
    else:
        break
print("You would need to save",str(portion_saved*100)+"% a month to afford the down payment on a $1M house on your salary in 3 years.")
print("This took", bisection_times, "steps in bisection search.")
