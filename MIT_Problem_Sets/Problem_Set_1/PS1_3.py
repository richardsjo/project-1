#Calculating how long it would take for someone to be able to afford a down payment on their dream house (price = total_cost) assuming they save a "portion_saved" percentage of their annual salary every month and taking into account annual return on investments.

def savetime(annual_salary,portion_saved,total_cost,current_savings,semi_annual_raise):
    i=0
    while True:
        i+=1
        current_savings+=current_savings*0.04/12
        current_savings+=portion_saved*annual_salary/12
        if current_savings > total_cost*0.25:
            return i
        if i%6==0:
            annual_salary+=semi_annual_raise*annual_salary

current_savings=0
annual_salary = float(input("What is your annual salary? "))
portion_saved = float(input("What is the percentage you want to save per month in decimal form e.g. 10% = 0.1. "))
total_cost = float(input("How much is your dream house? "))
semi_annual_raise=float(input("What percentage will your salary increase by every 6 months in decimal form?"))
i = savetime(annual_salary,portion_saved,total_cost,current_savings,semi_annual_raise)
print("You would need to save for", i , "months to be able to afford a down payment for your dream house.")
if i>=12:
    if i%12==0:
        print("This is equivalent to", i//12, "years")
    else:
        print("This is equivalent to", (i-i%12)//12, "years and", i%12, "months.")
