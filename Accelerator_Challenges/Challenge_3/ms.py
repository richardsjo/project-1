import ast
from operator import itemgetter
import itertools


#print(numbered_squares)

def probabilities(empty_space,array,probability):
    for i in empty_space:
        #print(array[2])
        #print(array[3])
        probability[str(i[0])+str(i[1])]+=float(array[2])/float(array[3])
        #print(str(i[0]))
        #print(str(i[1]))
        #print(probability[str(i[0])+str(i[1])])

def surrounding_squares(numbered_squares, grid):
    empty_space = []
    probability={}
    for i in range(len(grid)):
        for j in range(len(grid)):
            probability[str(i)+str(j)]=0
    for array in numbered_squares:
        counter = 0
        row_index = array[0]
        column_index = array[1]
        test=[]
        #print(row_index,column_index, 'indexes')

        for i in range(row_index-1, row_index+2):
            for j in range(column_index-1, column_index+2):
                if j >= 0 and i >= 0 and j < 9 and i < 9:
                    if grid[i][j] == ' ':
                        empty_space.append([i,j])
                        test.append([i,j])
                        counter+=1
        array.append(counter)
        probabilities(test,array,probability)


    empty_space_numbered = []
    # for array in empty_space:
    empty_space.sort()
    #print(empty_space)
    counts=[]
    for i in range(len(empty_space)):
        count = empty_space.count(empty_space[i])
        if empty_space[i]!=empty_space[i-1]:
            counts.append(count)
        empty_space_numbered.append([array, count])
    empty_space=list(empty_space for empty_space,_ in itertools.groupby(empty_space))
    #print(len(empty_space))
    #print(len(counts))
    for i in range(len(empty_space)):
        try:
            probability[str(empty_space[i][0])+str(empty_space[i][1])]/=counts[i]
        except IndexError:
            break
        # count = empty_space.count(array)
        # if count > 1:


        #empty_space = filter(lambda empty_space: empty_space != [array], empty_space)

    #empty_space = set(empty_space)
    #print(empty_space)
    return numbered_squares, empty_space_numbered,probability
def solver():
    minesweeper = open('map_array.txt', 'r')
    count=0
    grid = []
    for row in minesweeper:
        grid.append(ast.literal_eval(row))
    #print(list)

    numbered_squares = []
    for row_index in range(len(grid[0])):
        for column_index in range(len(grid[0])):
            if grid[row_index][column_index] != '0' and grid[row_index][column_index] != ' ' and grid[row_index][column_index] !="F":
            #print(row_index, column_index)
                numbered_squares.append([row_index, column_index, grid[row_index][column_index]])
            if grid[row_index][column_index]==' ':
                count+=1

    solution = surrounding_squares(numbered_squares,grid)
    solution[1].sort(key=itemgetter(1))
    for i in range(9):
        for j in range(9):
            if solution[2][str(i)+str(j)]==0:
                solution[2][str(i)+str(j)]=10000
#print(solution[2])
    min_prob=min(solution[2],key=solution[2].get)
#print(min_prob)
    dictionary = {0:'a', 1:'b', 2:'c', 3:'d', 4:'e', 5:'f', 6:'g', 7:'h', 8:'i'}



    output_array = open('input.txt', 'w')
    if count>10:
        output_array.write(str(dictionary[int(min_prob[1])])+str(int(min_prob[0])+1))
    else:
        output_array.write(str(dictionary[int(min_prob[1])])+str(int(min_prob[0])+1)+"f")
    output_array.close()
    return
    #print(dictionary[int(min_prob[1])],int(min_prob[0])+1, 'these are empties! PICK any of these!!')


    #print()
