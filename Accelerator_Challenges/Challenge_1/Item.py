class Item(object):
    def __init__(self,n,v,s,f):
        self.name=n
        self.value = v
        self.size = s
        self.IfFull = f
    def getName(self):
        return self.name
    def getValue(self):
        return self.value
    def getSize(self):
        return self.size
    def getIfFull(self):
        return self.IfFull
    def __str__(self):
        result = '<',self.name,',',self.value,',',self.size,'>'
    def changeIfFull(self,e):
        self.IfFull = e
    def changeSize(self,e):
        self.size=e
