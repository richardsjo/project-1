def fibeven(a,b,ans):
    mid = a+b
    if mid%2==0:
        return mid,mid
    else:
        return 0,mid

import time
start = time.time()
a=1
b=2
ans = 2
while True:
    test,a=fibeven(a,b,ans)
    ans+=test
    if a > int(4e6):
        break
    test,b=fibeven(a,b,ans)
    ans+=test
    if b > int(4e6):
        break
print(ans)
print(time.time()-start)            
