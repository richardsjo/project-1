# Finding the sum of the squares of the first 100 natural numbers and square of the sum of the first 100 m=natural numbers. Then doing one minus the other.

sumofsquares = 0
squareofsum = 0
for i in range(1,101):
    sumofsquares+=i**2
    squareofsum+=i
squareofsum=squareofsum**2
print(str(squareofsum) + "  - " + str(sumofsquares) +  " = " +  str(squareofsum-sumofsquares))
