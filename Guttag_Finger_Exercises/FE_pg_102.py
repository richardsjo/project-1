def sumDigits(s):
    #Assumes s is a string
    #Returns the sum of the decimal digits in s
    #For example, if s is "a2b3c" it returns 5
   ans=0
   for i in s:
       try:
           ans +=int(i)
       except ValueError:
           continue
   return ans

ans = sumDigits(input("Type a string made up of letters and integers."))
print(ans)
