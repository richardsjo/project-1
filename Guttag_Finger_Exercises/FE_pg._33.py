x=-25
if x<0:
    y=-x
else:
    y=x 
epsilon=0.01
numGuesses=0
low=0.0
high=max(1.0,y)
ans=(high+low)/2.0  
while abs(ans**3 - y) >=epsilon:
    print('low =', low, 'high =', high, 'ans =', ans)
    numGuesses +=1
    if ans**3 <y:
        low = ans
    else:
        high = ans
    ans = (high + low)/2.0    
print('numGuesses=', numGuesses)
if x<0:
    ans=-ans
print(ans, 'is close to cube root of', x)