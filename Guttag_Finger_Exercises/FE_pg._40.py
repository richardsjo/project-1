#Create a function isIn that finds whether one out of two strings is inside of the other.
import time

def isIn(x,y):
    for i in range(max(len(x)+1,len(y)+1)):
       for j in range(max(len(x)+1,len(y)+1)):
           check = x[i:j]
           if check == '':
               continue
           if check.lower() == y.lower():
               return y + " is in " + x
    for i in range(max(len(x)+1,len(y)+1)):
        for j in range(max(len(x)+1,len(y)+1)):
            check=y[i:j]
            if check == '':
                continue
            if check.lower() == x.lower():
                return x + " is in " + y
    return "Neither string is inside the other."

x = input("Enter a word/sentence.")
y = input("Enter another word/sentence.")
start = time.time()
ans = isIn(x,y)
print(ans)
print(time.time()-start)